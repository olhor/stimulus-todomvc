import Backbone from "backbone"

export default class Greeter extends Backbone.Model {
  get defaults() {
    return {
      greet_message: 'Hello, Backbone!',
      bye_message: 'Bye, bye!'
    }
  }

  constructor(attrs = {}, opts = {}) {
    super(attrs, opts)
    console.log('init hello')
  }

  greet() {
    console.log(this.get('greet_message'))
  }
}
