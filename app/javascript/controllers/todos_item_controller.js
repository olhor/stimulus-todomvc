import {Controller} from "stimulus"
import $ from 'jquery'

export default class extends Controller {
  static targets = ['name', 'completed', 'updateForm', 'destroy', 'id']

  set locked(val) {
    this.data.set('locked', val)
  }

  get locked() {
    return this.data.get('locked') === 'true'
  }

  get completed() {
    return $(this.completedTarget).is(':checked')
  }

  get active() {
    return !this.completed
  }

  get name() {
    return $(this.nameTarget).val()
  }

  get id() {
    return this.idTarget.value
  }

  get todosController() {
    return this.application.getControllerForElementAndIdentifier($('#todos')[0], 'todos');
  }

  connect() {
    console.log('item connected')
    const selectedFiler = this.todosController.filter
    if (selectedFiler === 'completed' && this.active || selectedFiler === 'active' && this.completed) {
      $(this.element).hide()
    }
    $(this.updateFormTarget).on('ajax:beforeSend', this.updateFormBeforeSend.bind(this))
    $(this.updateFormTarget).on('ajax:success', this.updateFormSuccess.bind(this))
    $(this.updateFormTarget).on('ajax:error', this.updateFormError.bind(this))
  }

  check() {
    console.log('check')
    this._triggerUpdate()
    this.completed ? this.todosController.completedCount += 1 : this.todosController.completedCount -= 1
  }

  lock() {
    this.locked = true
    let $input = $(this.element).find('input[type="text"]')
    $input.prop('readonly', this.locked)
    $input.blur()
  }

  unlock() {
    this.locked = false
    let $input = $(this.element).find('input[type="text"]')
    $input.prop('readonly', this.locked)
    $input.focus()
  }

  updateFormBeforeSend(e) {
    console.log('beforeSend')
    if (this.name.length === 0) {
      e.preventDefault()
      this._triggerDestroy()
    }
  }

  updateFormSuccess(e) {
    console.log('success')
    let template = this._response(e)
    $(this.element).replaceWith(template)
    this.todosController.errorsTarget.innerHTML = ''
  }

  updateFormError(e){
    this.todosController.errorsTarget.innerHTML = this._response(e)
  }

  _triggerUpdate() {
    //$(this.updateFormTarget).submit()
    Rails.fire(this.updateFormTarget, 'submit')
  }

  _triggerDestroy() {
    this.destroyTarget.click()
    $(this.element).remove()
    this.todosController.allCount -= 1
    if (this.completed) this.todosController.completedCount -= 1
  }

  // XXX: wtf rails-ujs is changing this into some monster...
  _response(e){
    return e.detail[2].response
  }
}
