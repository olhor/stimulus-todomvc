import {Controller} from "stimulus"
import $ from 'jquery'

export default class extends Controller {
  static targets = ["newTodoName", "todoItem", "filter", "errors", "addTodoForm", "list", "completed", "all"]

  set filter(val) {
    const el = this.filterTargets.find((el) => el.value === val)
    $(el).click()
  }

  get filter() {
    const el = this.filterTargets.find((el) => $(el).is(':checked'))
    return el.value
  }

  set completedCount(val) {
    $(this.completedTarget).text(val)
  }

  get completedCount() {
    return parseInt($(this.completedTarget).text())
  }

  set allCount(val) {
    $(this.allTarget).text(val)
  }

  get allCount() {
    return parseInt($(this.allTarget).text())
  }

  connect(){
    $(this.addTodoFormTarget).on('ajax:success', this.addTodo.bind(this))
    $(this.addTodoFormTarget).on('ajax:error', this.showErrors.bind(this))
  }

  /**
   * Removes completed todos.
   */
  removeCompleted() {
    console.log('removeCompleted')
    let ids = []
    this.todoItemTargets.forEach((el) => {
      let controller = this.todosItemController(el)
      if (controller.completed) {
        ids.push(controller.id)
        console.log('removing ' + controller.name)
        $(controller.element).remove()
        this.completedCount -= 1
        this.allCount -= 1
      }
    })
    const url = this.data.get('destroyManyUrl')
    $.ajax(url, {
      data: {ids: ids},
      type: 'DELETE',
      dataType: 'script',
      headers: {
        'X-CSRF-Token': Rails.csrfToken()
      }
    })
  }

  /**
   * Applies the currently selected filter.
   */
  applyFilter() {
    console.log('showing ' + this.filter)
    this.todoItemTargets.forEach((el) => {
      let controller = this.todosItemController(el)
      if (this.filter === 'all') {
        $(el).show()
      } else if (this.filter === 'active') {
        controller.active === true ? $(el).show() : $(el).hide()
      } else { // 'completed' filter
        controller.active === false ? $(el).show() : $(el).hide()
      }
    })
  }

  addTodo(e) {
    console.log('addTodo')
    let $elem = $(this._response(e))
    $elem.appendTo(this.listTarget)
    this.newTodoNameTarget.value = ''
    this.errorsTarget.innerHTML = ''
    this.allCount += 1
  }

  showErrors(e){
    console.log('showErrors', e)
    this.errorsTarget.innerHTML = this._response(e)
  }

  todosItemController(elem) {
    return this.application.getControllerForElementAndIdentifier(elem, 'todos-item')
  }

  // XXX: wtf rails-ujs is changing this into some monster...
  _response(e) {
    return e.detail[2].response
  }
}
