// Visit The Stimulus Handbook for more details 
// https://stimulusjs.org/handbook/introduction
// 
// This example controller works with specially annotated HTML like:
//
// <div data-controller="hello">
//   <h1 data-target="hello.output"></h1>
// </div>

import { Controller } from "stimulus"
import Greeter from "../models/greeter";

export default class extends Controller {
  static targets = [ "output" ]

  initialize() {
    this.greeter = new Greeter();
    this.greeter.greet()
  }

  connect() {
    this.greeter.set('greet_message', 'Hello, Backbone + Stimulus!')
  }

  greet(){
    this.outputTarget.textContent = this.greeter.get('greet_message')
  }

  bye(){
    this.outputTarget.textContent = this.greeter.get('bye_message')
  }
}
