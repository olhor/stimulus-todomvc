class TodosController < ApplicationController
  before_action :set_todo, only: [:update, :destroy]

  def index
    @todos = Todo.all
  end

  def create
    @todo = Todo.new(todo_params)

    if @todo.save
      render @todo, status: :created
    else
      render inline: errors, status: :unprocessable_entity
    end

  end

  def update
    @todo.assign_attributes(todo_params)
    if @todo.save
      render @todo
    else
      render inline: errors, status: :unprocessable_entity
    end
  end

  def destroy
    @todo.destroy
  end

  def destroy_many
    Todo.where(id: params[:ids]).destroy_all
    head :no_content
  end

  private

  def set_todo
    @todo = Todo.find(params[:id])
  end


  def todo_params
    params.require(:todo).permit!
  end

  def errors
    @todo.errors.full_messages.join(', ')
  end
end
