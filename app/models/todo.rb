class Todo < ApplicationRecord
  scope :active, -> {where(completed: false)}
  scope :completed, -> {where(completed: true)}

  validates :name, presence: true
end
