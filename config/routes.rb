Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'hello' => 'application#hello'
  resources 'todos' do
    delete 'destroy_many', on: :collection
  end
end
